#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include <st7565r.h>
#include <font.h>

void clear_screen(void) {

	uint8_t column = 0;
	uint8_t page = 0;

	lcd_cmd(START_LINE | (START_LINE_MASK & 0x00));

	for(page = 0; page < 9; page++) {
		for(column = 0; column < 128; column++)
			lcd_write(0x00);
		lcd_cmd(COLUMN_ADDRESS_LOW | (COLUMN_ADDRESS_MASK & 0x00));
		lcd_cmd(COLUMN_ADDRESS_HIGH | (COLUMN_ADDRESS_MASK & 0x00));
		lcd_cmd(PAGE_ADDRESS | (PAGE_ADDRESS_MASK & page));
	}
}

void write_authors_name(void) {
	
	lcd_cmd(COLUMN_ADDRESS_LOW | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(COLUMN_ADDRESS_HIGH | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(PAGE_ADDRESS | (PAGE_ADDRESS_MASK & 0x00));

	lcd_putc('A');
	lcd_putc('l');
	lcd_putc('e');
	lcd_putc('x');
	lcd_putc('a');
	lcd_putc('n');
	lcd_putc('d');
	lcd_putc('e');
	lcd_putc('r');
	
	lcd_cmd(COLUMN_ADDRESS_LOW | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(COLUMN_ADDRESS_HIGH | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(PAGE_ADDRESS | (PAGE_ADDRESS_MASK & 0x01));

	lcd_putc('M');
	lcd_putc('e');
	lcd_putc('i');
	lcd_putc('n');
	lcd_putc('k');
	lcd_putc('e');
	lcd_putc('!');
	
	lcd_cmd(COLUMN_ADDRESS_LOW | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(COLUMN_ADDRESS_HIGH | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(PAGE_ADDRESS | (PAGE_ADDRESS_MASK & 0x02));
	
	lcd_putc('@');
	lcd_putc('+');
	lcd_putc('*');
	lcd_putc('~');
	lcd_putc('#');
	lcd_putc('"');
	lcd_putc(',');
	lcd_putc(';');
	lcd_putc('.');
	lcd_putc(':');
	lcd_putc('-');
	lcd_putc('_');
	
	lcd_cmd(COLUMN_ADDRESS_LOW | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(COLUMN_ADDRESS_HIGH | (COLUMN_ADDRESS_MASK & 0x00));
	lcd_cmd(PAGE_ADDRESS | (PAGE_ADDRESS_MASK & 0x03));

	lcd_putc('$');
	lcd_putc('%');
	lcd_putc('&');
	lcd_putc('/');
	lcd_putc('{');
	lcd_putc('}');
	lcd_putc('[');
	lcd_putc(']');
	lcd_putc('?');
	lcd_putc('=');
}

void draw(void) {

	uint8_t testBitmap[2][16] = {{0x0F,0xF0,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
								0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF},
								{0x0F,0xF0,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
								0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}};
	
	lcd_cmd(START_LINE | (START_LINE_MASK & 0x00));
	
	show_icon((uint8_t*)testBitmap,16,16,1,20,1);
	show_icon((uint8_t*)testBitmap,16,16,1,3,1);
	show_icon((uint8_t*)testBitmap,16,16,17,3,1);

	show_icon((uint8_t*)font['M'-32],8,8,50,3,1);
	show_icon((uint8_t*)font['y'-32],8,8,58,3,1);
	show_icon((uint8_t*)font['A'-32],8,8,66,3,1);
	show_icon((uint8_t*)font['V'-32],8,8,74,3,1);
	show_icon((uint8_t*)font['R'-32],8,8,82,3,1);
	
	show_icon((uint8_t*)testBitmap,16,8,50,13,1);
	show_icon((uint8_t*)testBitmap,16,8,66,13,1);

	show_icon((uint8_t*)font['M'-32],8,8,74,23,1);
	show_icon((uint8_t*)font['k'-32],8,8,82,23,1);
	show_icon((uint8_t*)font['3'-32],8,8,90,23,1);
	show_icon((uint8_t*)font['!'-32],8,8,98,23,1);
}

int main(void) {

	lcd_init();
	clear_screen();

	// write_authors_name();
	draw();	

	DDRL = 0xFF;

	while(1) {
		PORTL++;
		_delay_ms(500);
	}

	return 0;
}
