CC=/usr/bin/avr-gcc
OBJCPY=/usr/bin/avr-objcopy
AVRDUDE=/usr/bin/avrdude
RM=/bin/rm
MKDIR=/bin/mkdir

MCU=atmega2560

#
# Some Linux distribution require explicit declaration of linker script.
# See http://bugs.gentoo.org/show_bug.cgi?id=147155
#
LD_SCRIPT=/usr/lib/binutils/avr/2.20/ldscripts/avr6.x

F_CPU=16000000

#
# Would be nice to define FONT like this:
# FONT=default
#

FORMAT=ihex

PART=m2560
PROGRAMMER=stk500pp
PORT=/dev/ttyUSB0

###############################################################################

CFLAGS=-Os -mmcu=$(MCU) \
		-DF_CPU=$(F_CPU) -DFONT_DEFAULT \
		-I/usr/avr/include -Iinclude \
		-T$(LD_SCRIPT) -Wall

HEXFLAGS=-O$(FORMAT) -R .eeprom -R .fuse -R .lock

DUDEFLAGS=-p $(PART) -c $(PROGRAMMER) -P $(PORT)

###############################################################################

TARGET=example
SRC=example.c drivers/st7565r.c
BUILD=build

###############################################################################

all:
	-$(MKDIR) $(BUILD)
	$(CC) $(CFLAGS) $(SRC) -o $(BUILD)/$(TARGET).elf
	$(OBJCPY) $(HEXFLAGS) $(BUILD)/$(TARGET).elf $(BUILD)/$(TARGET).ihex

#
# Please note PROGRAMMER setting.
#
program:
	$(AVRDUDE) $(DUDEFLAGS) -U flash:w:$(BUILD)/$(TARGET).ihex:i

clean:
	$(RM) $(BUILD)/$(TARGET).*
